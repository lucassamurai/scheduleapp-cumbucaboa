using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScheduleApi.Data;
using ScheduleApi.Models;

[Route("v1/Exceptions")]
public class ExceptionsController : Controller
{
  [HttpGet]
  [Route("")]
  [Authorize]
  public async Task<ActionResult<List<Exceptions>>> Get([FromServices] DataContext context)
  {
    var exceptions = await context.Exceptions.Include(x => x.Period).AsNoTracking().ToListAsync();
    return exceptions;
  }

  [HttpGet]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<Exceptions>> GetById([FromServices] DataContext context, int id)
  {
    var exceptions = await context.Exceptions.Include(x => x.Period).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
    return exceptions;
  }

  [HttpGet]
  [Route("periods/{id:int}")]
  [Authorize]
  public async Task<ActionResult<List<Exceptions>>> GetByPeriod([FromServices] DataContext context, int id)
  {
    var exceptions = await context.Exceptions.Include(x => x.Period).Where(x => x.Period.Id == id).AsNoTracking().ToListAsync();
    return exceptions;
  }

  [HttpPost]
  [Route("")]
  [Authorize]
  public async Task<ActionResult<Exceptions>> Post(
      [FromServices] DataContext context,
      [FromBody]Exceptions model)
  {
    if (ModelState.IsValid)
    {
      context.Exceptions.Add(model);
      await context.SaveChangesAsync();
      return model;
    }
    else
    {
      return BadRequest(ModelState);
    }
  }

  [HttpPut]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<List<Exceptions>>> Put(
    int id,
    [FromBody]Exceptions model,
    [FromServices]DataContext context)
  {
    var Exception = await context.Exceptions.FirstOrDefaultAsync(x => x.Id == id);
    if (Exception == null)
      return NotFound(new { message = "Exceção não encontrado" });

    try
    {
      Exception.LastUpdate = DateTime.Now;
      Exception.Status = model.Status;
      context.Entry(Exception).State = EntityState.Modified;
      await context.SaveChangesAsync();
      return Ok(Exception);
    }
    catch (DbUpdateConcurrencyException)
    {
      return BadRequest(new { message = "Este registro já foi atualizado" });
    }
    catch (System.Exception)
    {
      return BadRequest(new { message = "Não foi possível atualizar a exceção" });
    }
  }

  [HttpDelete]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<Exceptions>> Delete(
          [FromServices] DataContext context,
          int id)
  {
    var exception = await context.Exceptions.FirstOrDefaultAsync(x => x.Id == id);
    if (exception == null)
      return NotFound(new { message = "Exceção não encontrada" });

    try
    {
      context.Exceptions.Remove(exception);
      await context.SaveChangesAsync();
      return Ok(new { message = "Exceção removida com sucesso" });
    }
    catch (Exception)
    {
      return BadRequest(new { message = "Não foi possível remover a Exceção" });
    }
  }
}