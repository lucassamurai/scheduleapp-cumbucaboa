using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ScheduleApi.Data;
using ScheduleApi.Models;

namespace Backoffice.Controllers
{
  [Route("v1")]
  public class HomeController : Controller
  {
    [HttpGet]
    [Route("")]
    public async Task<ActionResult<dynamic>> Get([FromServices] DataContext context)
    {
      var employee = new User { Username = "operador", Email = "operador@samuraiexperts.com.br", Password = "123456", Role = "employee" };
      var manager = new User { Username = "manager", Email = "manager@samuraiexperts.com.br", Password = "123456", Role = "manager" };
      context.Users.Add(employee);
      context.Users.Add(manager);
      await context.SaveChangesAsync();

      return Ok(new
      {
        message = "Dados configurados"
      });
    }
  }
}