using System;
using System.ComponentModel.DataAnnotations;

namespace ScheduleApi.Models
{
  public class ShippingRange
  {
    [Key]
    public int Id { get; set; }

    public string StartingRange { get; set; }

    public string FinalRange { get; set; }

    public DateTime InsertDate { get; set; }

    public DateTime LastUpdate { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [Range(1, int.MaxValue, ErrorMessage = "Região inválida")]
    public int RegionId { get; set; }
    public Region Region { get; set; }

    public StatusEnum.Estatus Status { get; set; }
  }
}