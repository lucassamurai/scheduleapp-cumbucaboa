﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleApi.Models
{
    public class Order
    {
        [JsonProperty("id")]
        public long id { get; set; }
        [JsonProperty("note_attributes")]
        public string NoteAttributes { get; set;
}     
    }
}
