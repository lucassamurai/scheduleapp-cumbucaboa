using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScheduleApi.Models
{
  public class Period
  {
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

    public string CoverageOf { get; set; }

    public string CoverageUntil { get; set; }

    public string PeriodOf  { get; set; }

    public string PeriodUntil   { get; set; }

    public string DeliveryType { get; set; }

    [ForeignKey("Region")]
    public int RegionId { get; set; }

    public Region Region { get; set; }

    public DateTime InsertDate { get; set; }

    public DateTime LastUpdate { get; set; }

    public StatusEnum.Estatus Status { get; set; }
  }
}