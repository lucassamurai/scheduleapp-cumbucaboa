﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleApi.Models
{
    public class ShopifyShop
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string JsonShop { get; set; }
        public string IdShopify { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string Address1 { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string CountryName { get; set; }
        public string MyshopifyDomain { get; set; }
        public string Code { get; set; }
        public string AccessToken { get; set; }
    }
}
