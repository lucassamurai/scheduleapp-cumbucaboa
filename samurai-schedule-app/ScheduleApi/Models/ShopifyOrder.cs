﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleApi.Models
{
    public class ShopifyOrder
    {
        public Order order { get; set; }
        public class Order
        {
            public long id { get; set; }
            public List<Note> note_attributes { get; set; }
        }
        public class Note
        {
            public string name { get; set; }
            public string value { get; set; }
        }
    }
}
