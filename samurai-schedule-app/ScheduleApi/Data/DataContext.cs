using Microsoft.EntityFrameworkCore;
using ScheduleApi.Models;

namespace ScheduleApi.Data
{
  public class DataContext : DbContext
  {
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Rules>().HasOne(e => e.Period).WithMany().Metadata.DeleteBehavior = DeleteBehavior.Restrict;
      modelBuilder.Entity<Rules>().HasOne(e => e.DeliveryPeriod).WithMany().Metadata.DeleteBehavior = DeleteBehavior.Restrict;

      base.OnModelCreating(modelBuilder);
    }

    public DbSet<Region> Regions { get; set; }
    public DbSet<ShippingRange> Ranges { get; set; }
    public DbSet<Period> Periods { get; set; }
    public DbSet<Rules> Rules { get; set; }
    public DbSet<Holiday> Holidays { get; set; }
    public DbSet<Exceptions> Exceptions { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<ShopifyShop> ShopifyShops { get; set; }
  }
}
